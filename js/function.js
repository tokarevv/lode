;(function() {
	'use strict';
	/*
	0 - prazdne misto
	1 - kousek lodi
	2 - pole vedle lodi
	3 - postrelene pole
	4 - zastrelena paluba
	*/

	
	// flag pro zacatek hry, nastavuje se po zmacknuti 'play' a zakazuje menit polohu lodi
	let startGame = false;
	// flag rucniho nastaveni lodi a jejich polohy
	let isHandlerPlacement = false;
	// flag nastaveni manipulatoru hry
	let isHandlerController = false;
	// flag zakazujici tah hrace, kdyz hraje pocitac
	let compShot = false;

	// dostavam DOM podle jeho ID
	const getElement = id => document.getElementById(id);
	// vypocet coordinatu vsech stran elementu v souvislosti s oknem browseru s ohledem na rolovani stranky
	const getCoordinates = el => {
		const coords = el.getBoundingClientRect();
		return {
			left: coords.left + window.pageXOffset,
			right: coords.right + window.pageXOffset,
			top: coords.top + window.pageYOffset,
			bottom: coords.bottom + window.pageYOffset
		};
	};

	// herni pole cloveka
	const humanfield = getElement('field_human');
	// herni pole pocitace
	const computerfield = getElement('field_computer');

	class Field {
		// velikost herniho pole v pixelech
		static FIELD_SIDE = 330;
		// velikost paluby v pixelech
		static SHIP_SIDE = 33;
		// objekt s udaji o lodich
		// klic je typ lodi a hodnota je pole, prvni cislo je pocet lodi, druhe cislo je pocet palub lodi takoveho typu
		static SHIP_DATA = {
			fourdeck: [1, 4],
			tripledeck: [2, 3],
			doubledeck: [3, 2],
			singledeck: [4, 1]
		};

		constructor(field) {
			// objekt herniho pole jako argument
			this.field = field;
			// prazdny objekt pro naplneni udaji o kazde lodi z eskadry
			this.squadron = {};
			// dvourozmerne pole, do ktereho se zadavaji souradnice lodi, a, behem hry, souradnice zasahu, minuti a prazdnych bunek
			this.matrix = []; 
			// souradnice vsech 4 stran ramce herniho herniho pole oproti zacatku document, vhledem k vertikalnimu skrolovani
			let { left, right, top, bottom } = getCoordinates(this.field);
			this.fieldLeft = left;
			this.fieldRight = right;
			this.fieldTop = top;
			this.fieldBottom = bottom;
		}

		static createMatrix() {
			return [...Array(10)].map(() => Array(10).fill(0));
		}
		//  n = maximalni hodnota, kterou chceme dostat
		static getRandom = n => Math.floor(Math.random() * (n + 1));

		cleanField() {
			while (this.field.firstChild) {
				this.field.removeChild(this.field.firstChild);
			}
			this.squadron = {};
			this.matrix = Field.createMatrix();
		}

		randomLocationShips() {
			for (let type in Field.SHIP_DATA) {
				// pocet lodi takoveho druhu
				let count = Field.SHIP_DATA[type][0];
				// pocet palub u lodi takoveho druhu
				let decks = Field.SHIP_DATA[type][1];
				
				for (let i = 0; i < count; i++) {
					// souradnice prvni paluby lodi a smerovani
					let options = this.getCoordsDecks(decks);
					// pocet palub
					options.decks = decks;
					// jmeno lodi pro identifikaci
					options.shipname = type + String(i + 1);
					// instance lodi s vlastnostmi zadanymi v objektu options pomocí tridy Ship
					const ship = new Ships(this, options);
					ship.createShip();
				}
			}
		}

		getCoordsDecks(decks) {
			// získáme koeficienty, které určují směr umístění lodi
 			// kx == 0 a ky == 1 - loď je umístěna vodorovně,
 			// kx == 1 a ky == 0 - vertikální.
			let kx = Field.getRandom(1), ky = (kx == 0) ? 1 : 0,
				x, y;

			// na zaklade smeru umisteni, generuje pocatecni souradnice
			if (kx == 0) {
				x = Field.getRandom(9); y = Field.getRandom(10 - decks);
			} else {
				x = Field.getRandom(10 - decks); y = Field.getRandom(9);
			}

			const obj = {x, y, kx, ky}
			// overeni validity vsech souradnic lodi
			const result = this.checkLocationShip(obj, decks);
			// kdyz nejsou validni, znovu
			if (!result) return this.getCoordsDecks(decks);
			return obj;
		}

		checkLocationShip(obj, decks) {
			let { x, y, kx, ky, fromX, toX, fromY, toY } = obj;

			// indexy pro dvourozmerne pole
			fromX = (x == 0) ? x : x - 1;
			if (x + kx * decks == 10 && kx == 1) toX = x + kx * decks;
			else if (x + kx * decks < 10 && kx == 1) toX = x + kx * decks + 1;
			else if (x == 9 && kx == 0) toX = x + 1;
			else if (x < 9 && kx == 0) toX = x + 2;

			// indexy zacatku a konce volby podle sloupcu
			fromY = (y == 0) ? y : y - 1;
			if (y + ky * decks == 10 && ky == 1) toY = y + ky * decks;
			else if (y + ky * decks < 10 && ky == 1) toY = y + ky * decks + 1;
			else if (y == 9 && ky == 0) toY = y + 1;
			else if (y < 9 && ky == 0) toY = y + 2;

			if (toX === undefined || toY === undefined) return false;

			// filtrovani bunek dvourozmerneho pole
			// pokud bunka obsahuje 1 - vrati false
			if (this.matrix.slice(fromX, toX)
				.filter(arr => arr.slice(fromY, toY).includes(1))
				.length > 0) return false;
			return true;
		}
	}

	///////////////////////////////////////////

	class Ships {
		constructor(self, { x, y, kx, ky, decks, shipname }) {
			this.player = (self === human) ? human : computer;
			// this.player = self;
			// v jakem poli vytvarime lod
			this.field = self.field;
			// unikatni jmeno lodi
			this.shipname = shipname;
			// pocet palub
			this.decks = decks;
			// souradnice x prvni paluby
			this.x = x;
		 	// souradnice y prvni paluby
			this.y = y;
			// smer palub
			this.kx = kx;
			this.ky = ky;
			// pocet zasahu
			this.hits = 0;
			// pole se souradnicemi palub lodi je elementem squadron
			this.arrDecks = [];
		}

		static showShip(self, shipname, x, y, kx) {
			// novy prvek se zadanym tegem
			const div = document.createElement('div');
			// odranuje z nazvu lodi cisla a ziskava nazev tridy
			const classname = shipname.slice(0, -1);
			// ziska nazev tridy v zavislosti na smer umisteni lodi
			const dir = (kx == 1) ? ' vertical' : '';

			// unikatni identifikator lodi
			div.setAttribute('id', shipname);
			// shromazduje vsechny tridy do jednoho radku
			div.className = `ship ${classname}${dir}`;
			// pres atribut "style" nastavi umisteni lodi vzhledem k rodicovskemu prvku
			// offset se spocita vynasobenim souradnic prvni paluby a velikosti bunky herniho pole, velikost se shoduej s velikosti paluby
			div.style.cssText = `left:${y * Field.SHIP_SIDE}px; top:${x * Field.SHIP_SIDE}px;`;
			self.field.appendChild(div);
		}

		createShip() {
			let { player, field, shipname, decks, x, y, kx, ky, hits, arrDecks, k = 0 } = this;

			while (k < decks) {
				let i = x + k * kx, j = y + k * ky;

				// 1 v bunce matrix znamena, ze je v bunce nejaka paluba lodi
				player.matrix[i][j] = 1;
				// zapise souradnice paluby
				arrDecks.push([i, j]);
				k++;
			}

			// zada info o lodi do objektu eskadry
			player.squadron[shipname] = {arrDecks, hits, x, y, kx, ky};
			// pokud lod je vytvorena pro hrace, zobrazi ji na obrazovce
			if (player === human) {
				Ships.showShip(human, shipname, x, y, kx);
				// kdyz pocet lodi v eskadre bude 10, vsechny lode jsou vygenerovane a zobrazi tlacitko startu
				if (Object.keys(player.squadron).length == 10) {
					buttonPlay.hidden = false;
				}
			}
		}
	}

	///////////////////////////////////////////

	class Placement {
		// objekt se souradnicemi stran herniho pole
		static FRAME_COORDS = getCoordinates(humanfield);
		
		constructor() {
			// objekt pretahovane lodi
			this.dragObject = {};
			// flag pro zmacknuti levyho tlacitka mysi
			this.pressed = false;
		}

		static getShipName = el => el.getAttribute('id');
		static getCloneDecks = el => {
			const type = Placement.getShipName(el).slice(0, -1);
			return Field.SHIP_DATA[type][1];
		}

		setObserver() {
			if (isHandlerPlacement) return;
			document.addEventListener('mousedown', this.onMouseDown.bind(this));
			document.addEventListener('mousemove', this.onMouseMove.bind(this));
			document.addEventListener('mouseup', this.onMouseUp.bind(this));
			humanfield.addEventListener('contextmenu', this.rotationShip.bind(this));
			isHandlerPlacement = true;
		}

		onMouseDown(e) {
			// pokud bylo zmacknute jine tlacitko nebo hra je jiz spustena
			if (e.which != 1 || startGame) return;

			// check zda zmacknuti bylo nad lodi
			const el = e.target.closest('.ship');
			if(!el) return;

			this.pressed = true;

			// pretahovany objekt a jeho vlasnosti
			this.dragObject = {
				el,
				parent: el.parentElement,
				next: el.nextElementSibling,
				// souradnice ze kterych je preneseny
				downX: e.pageX,
				downY: e.pageY,
				// souradnice left a top se pouzivaji pri uprave polohy lodi na hernim poli
				left: el.offsetLeft,
				top: el.offsetTop,
				// horizontalni poloha lodi
				kx: 0,
				ky: 1
			};

			// upravuje polohu lodi na hernim poli
			// overi ze lod se nachazi na hrenim poli hrace
			if (el.parentElement === humanfield) {
				const name = Placement.getShipName(el);
				// zapamatuje smer palub
				this.dragObject.kx = human.squadron[name].kx;
				this.dragObject.ky = human.squadron[name].ky;
			}
		}

		onMouseMove(e) {
			if (!this.pressed || !this.dragObject.el) return;

			// souradnice stran klonu lodi
			let { left, right, top, bottom } = getCoordinates(this.dragObject.el);

			// kdyz klon nexistuje, vytvori ho
			if (!this.clone) {
				// pocet palub pretahovane lodi
				this.decks = Placement.getCloneDecks(this.dragObject.el);
				// vytvori klon podle ziskanych souradnic jeho stran
				this.clone = this.creatClone({left, right, top, bottom}) || null;
				// kdyz se nepovedlo vytvorit klon, vraci se z funkce
				if (!this.clone) return;

				// vypocita posun kurzoru podle souradnic x a y
				this.shiftX = this.dragObject.downX - left;
				this.shiftY = this.dragObject.downY - top;
				// z-index je potreba k umisteni klonu nad vsemi elementy DOM
				this.clone.style.zIndex = '1000';
				// pretahne klon do BODY
				document.body.appendChild(this.clone);

				// smaze starou instanci lodi, pokud existuje pri uprave polohy lodi
				this.removeShipFromSquadron(this.clone);
			}

			//souradnice klonu v BODY s posunem kurzoru
			let currentLeft = Math.round(e.pageX - this.shiftX),
				currentTop = Math.round(e.pageY - this.shiftY);
			this.clone.style.left = `${currentLeft}px`;
			this.clone.style.top = `${currentTop}px`;

			//kontroluje yda je klon v hracim poli s chybou do 14 pixelu
			if (left >= Placement.FRAME_COORDS.left - 14 && right <= Placement.FRAME_COORDS.right + 14 && top >= Placement.FRAME_COORDS.top - 14 && bottom <= Placement.FRAME_COORDS.bottom + 14) {
				// клон находится в пределах игрового поля,
				// klon je v hernim poli, zvyrazni se zelene
				this.clone.classList.remove('unsuccess');
				this.clone.classList.add('success');

				const { x, y } = this.getCoordsCloneInMatrix({ left, right, top, bottom });
				const obj = {
					x,
					y,
					kx: this.dragObject.kx,
					ky: this.dragObject.ky
				};

				const result = human.checkLocationShip(obj, this.decks);
				if (!result) {
					// v sousednich bunkach se nachazi lod, zvyrazni cervene
					this.clone.classList.remove('success');
					this.clone.classList.add('unsuccess');
				}
			} else {
				// klon je mimo herni pole, zvyrazni se cervene
				this.clone.classList.remove('success');
				this.clone.classList.add('unsuccess');
			}
		}

		onMouseUp(e) {
			this.pressed = false;
			// pokud klon neexistuje
			if (!this.clone) return;

			// pokud souradnice klonu nejsou validni, vrati se na misto, odkud hrac ho pretahl
			if (this.clone.classList.contains('unsuccess')) {
				this.clone.classList.remove('unsuccess');
				this.clone.rollback();
			} else {
				// instance nove lodi podle finalnich souradnic klonu
				this.createShipAfterMoving();
			}

			// maze objekty 'clone' a 'dragObject'
			this.removeClone();
		}

		rotationShip(e) {
			// zakaz zobrazeni kontextoveho menu
			e.preventDefault();
			if (e.which != 3 || startGame) return;

			const el = e.target.closest('.ship');
			const name = Placement.getShipName(el);

			// nema cenu vracet lod s jednou palubou
			if (human.squadron[name].decks == 1) return;

			// objekt s aktualnimi koeficienty a souradnicemi lodi
			const obj = {
				kx: (human.squadron[name].kx == 0) ? 1 : 0,
				ky: (human.squadron[name].ky == 0) ? 1 : 0,
				x: human.squadron[name].x,
				y: human.squadron[name].y
			};
			// mazani dat o upravovane lodi
			const decks = human.squadron[name].arrDecks.length;
			this.removeShipFromSquadron(el);
			human.field.removeChild(el);

			// check na validitu novych souradnic po otaceni, kdyz nejsou validni, vraci stare
			const result = human.checkLocationShip(obj, decks);
			if(!result) {
				obj.kx = (obj.kx == 0) ? 1 : 0;
				obj.ky = (obj.ky == 0) ? 1 : 0;
			}

			// prida vlastnosti nove lodi k objektu
			obj.shipname = name;
			obj.decks = decks;

			// instance nove lodi
			const ship = new Ships(human, obj);
			ship.createShip();

			// kratce zvyrazni pole cervene
			if (!result) {
				const el = getElement(name);
				el.classList.add('unsuccess');
				setTimeout(() => { el.classList.remove('unsuccess') }, 750);
			}
		}

		creatClone() {
			const clone = this.dragObject.el;
			const oldPosition = this.dragObject;

			clone.rollback = () => {
				// uprava polohy lodi
				// vraceni lodi na puvodni misto na herni pole
				if (oldPosition.parent == humanfield) {
					clone.style.left = `${oldPosition.left}px`;
					clone.style.top = `${oldPosition.top}px`;
					clone.style.zIndex = '';
					oldPosition.parent.insertBefore(clone, oldPosition.next);
					this.createShipAfterMoving();
				} else {
					// vraci lod do 'shipsCollection'
					clone.removeAttribute('style');
					oldPosition.parent.insertBefore(clone, oldPosition.next);
				}
			};
			return clone;
		}

		removeClone() {
			delete this.clone;
			this.dragObject = {};
		}

		createShipAfterMoving() {
			// souradnice prepocitane vzhledem k hracimu poli
			const coords = getCoordinates(this.clone);
			let { left, top, x, y } = this.getCoordsCloneInMatrix(coords);
			this.clone.style.left = `${left}px`;
			this.clone.style.top = `${top}px`;
			// prenasi klon do herniho pole
			humanfield.appendChild(this.clone);
			this.clone.classList.remove('success');

			// objekt s vlastnostmi nove lodi
			const options = {
				shipname: Placement.getShipName(this.clone),
				x,
				y,
				kx: this.dragObject.kx,
				ky: this.dragObject.ky,
				decks: this.decks
			};

			// instance nove lodi
			const ship = new Ships(human, options);
			ship.createShip();
			// ted' je na pole samotna lod, takze ji smaze z DOM
			humanfield.removeChild(this.clone);
		}

		getCoordsCloneInMatrix({left, right, top, bottom} = coords) {
			// vypocita rozdil souradnic stran klonu a herniho pole
			let computedLeft = left - Placement.FRAME_COORDS.left,
				computedRight = right - Placement.FRAME_COORDS.left,
				computedTop = top - Placement.FRAME_COORDS.top,
				computedBottom = bottom - Placement.FRAME_COORDS.top;

			// objekt pro finalni hodnoty
			const obj = {};

			// po splneni podminek, odstrani nepresnosti v poloze klonu
			let ft = (computedTop < 0) ? 0 : (computedBottom > Field.FIELD_SIDE) ? Field.FIELD_SIDE - Field.SHIP_SIDE : computedTop;
			let fl = (computedLeft < 0) ? 0 : (computedRight > Field.FIELD_SIDE) ? Field.FIELD_SIDE - Field.SHIP_SIDE * this.decks : computedLeft;

			obj.top = Math.round(ft / Field.SHIP_SIDE) * Field.SHIP_SIDE;
			obj.left = Math.round(fl / Field.SHIP_SIDE) * Field.SHIP_SIDE;
			// prevadi hodnotu na souradnice matrix
			obj.x = obj.top / Field.SHIP_SIDE;
			obj.y = obj.left / Field.SHIP_SIDE;

			return obj;
		}

		removeShipFromSquadron(el) {
			// nazev upravovane lodi
			const name = Placement.getShipName(el);
			// pokud lod s takovym jmenem neexistuje, zastavuje funkci
			if (!human.squadron[name]) return;

			// pole se souradnicemi palub lodi, zapisuje tam nulu pro prazdne misto
			const arr = human.squadron[name].arrDecks;
			for (let coords of arr) {
				const [x, y] = coords;
				human.matrix[x][y] = 0;
			}
			// mazani veskere info o lodi z pole eskadry
			delete human.squadron[name];
		}
	}

	///////////////////////////////////////////

	class Controller {
		// pole zakladnich souradnic k vytvoreni coordsFixedHit
		static START_POINTS = [
			[ [6,0], [2,0], [0,2], [0,6] ],
			[ [3,0], [7,0], [9,2], [9,6] ]
		];
		// Blok, ve kterem se behem hry zobrazuji info zpravy
		static SERVICE_TEXT = getElement('service_text');

		constructor() {
			this.player = '';
			this.opponent = '';
			this.text = '';
			// pole pro souradnice vystrelu pri nahodne volbe
			this.coordsRandomHit = [];
			// pole s predem vypocitanymi souradnicemi vystrelu
			this.coordsFixedHit = [];
			// pole pro souradnice sousednych od vystrelu bunek
			this.coordsAroundHit = [];
			// temp objekt lodi, kam se zadavaji souradnice zasahu, poloha lodi a pocet zasahu
			this.resetTempShip();
		}

		// vystup pro info
		static showServiceText = text => {
			Controller.SERVICE_TEXT.innerHTML = text;
		}

		// prevod absolutnich souradnic ikonu na souradnice matrix
		static getCoordsIcon = el => {
			const x = el.style.top.slice(0, -2) / Field.SHIP_SIDE;
			const y = el.style.left.slice(0, -2) / Field.SHIP_SIDE;
			return [x, y];
		}

		// mazani nepotrebnych souradnic z pole
		static removeElementArray = (arr, [x, y]) => {
			return arr.filter(item => item[0] != x || item[1] != y);
		}

		init() {
			// nahodny vyber hrace a soupere
			const random = Field.getRandom(1);
			this.player = (random == 0) ? human : computer;
			this.opponent = (this.player === human) ? computer : human;

			// generace souradnic vystrelu pc a jejich prideleni do poli coordsRandomHit a coordsFixedHit
			this.setCoordsShot();

			// kontroler udalosti hrace
			if (!isHandlerController) {
				//vystrel hrace
				computerfield.addEventListener('click', this.makeShot.bind(this));
				// устанавливаем маркер на заведомо пустую клетку				// nastaveni markeru na prazdnou bunku
				computerfield.addEventListener('contextmenu', this.setUselessCell.bind(this));
				isHandlerController = true;
			}

			if (this.player === human) {
				compShot = false;
				this.text = 'Ty střílíš první';
			} else {
				compShot = true;
				this.text = 'Počítač střílí první';
				// vystrel pc
				setTimeout(() => this.makeShot(), 2000);
			}
			Controller.showServiceText(this.text);
		}

		setCoordsShot() {
			// souradnice kazde bunky hraciho pole a jejich zapis do pole
			for (let i = 0; i < 10; i++) {
				for(let j = 0; j < 10; j++) {
					this.coordsRandomHit.push([i, j]);
				}
			}
			// nahodne promichani pole se souradnicemi
			this.coordsRandomHit.sort((a, b) => Math.random() - 0.5);

			let x, y;

			// souradnice pro strelbu diagonalne vpravo a dolu
			for (let arr of Controller.START_POINTS[0]) {
				x = arr[0]; y = arr[1];
				while (x <= 9 && y <= 9) {
					this.coordsFixedHit.push([x, y]);
					x = (x <= 9) ? x : 9;
					y = (y <= 9) ? y : 9;
					x++; y++;
				}
			}

			// souradnice pro strelbu vpravo a nahoru
			for (let arr of Controller.START_POINTS[1]) {
				x = arr[0]; y = arr[1];
				while(x >= 0 && x <= 9 && y <= 9) {
					this.coordsFixedHit.push([x, y]);
					x = (x >= 0 && x <= 9) ? x : (x < 0) ? 0 : 9;
					y = (y <= 9) ? y : 9;
					x--; y++;
				};
			}
			// opacne poradi prvku, aby ostrelovani bylo v poradi podle 
			this.coordsFixedHit = this.coordsFixedHit.reverse();
		}

		setCoordsAroundHit(x, y, coords) {
			let {firstHit, kx, ky} = this.tempShip;

			// pole je praydne, tj. je to prvni zasah na lod
			if (firstHit.length == 0) {
				this.tempShip.firstHit = [x, y];
			// druhy zasah protoze oba koeficienty jsou 0
			} else if (kx == 0 && ky == 0) {
				// zname souradnice prvniho a druheho zasahu, spocitat smer lodi
				this.tempShip.kx = (Math.abs(firstHit[0] - x) == 1) ? 1 : 0;
				this.tempShip.ky = (Math.abs(firstHit[1] - y) == 1) ? 1 : 0;
			}

			// kontrola spravnosti prijatych souradnic ostrelovani
			for (let coord of coords) {
				x = coord[0]; y = coord[1];
				// souradnice za hernim polem
				if (x < 0 || x > 9 || y < 0 || y > 9) continue;
				// v tutich souradnichc je marker minuti nebo prazdne bunky
				if (human.matrix[x][y] != 0 && human.matrix[x][y] != 1) continue;
				// validni souradnice pridavame do martixu
				this.coordsAroundHit.push([x, y]);
			}
		}

		isShipSunk() {
			// max pocet palub u zbyvajicich lodi
			let obj = Object.values(human.squadron)
				.reduce((a, b) => a.arrDecks.length > b.arrDecks.length ? a : b);
			// check zda jsou jeste zive lode
			if (this.tempShip.hits >= obj.arrDecks.length || this.coordsAroundHit.length == 0) {
				// lod je potopena, kolem ni bude UselessCell
				this.markUselessCellAroundShip();
				// ocisteni pole coordsAroundHit a reset objektu TempShip, pro strileni na dalsi lod
				this.coordsAroundHit = [];
				this.resetTempShip();
			}
		}

		setUselessCell(e) {
			e.preventDefault();
			// check na zmacknuti praveho tl mysi a flagu, ktery blokuje akce hrace
			if (e.which != 3 || compShot) return;

			// preneseni souradnic kliku ve srovnani s oknem browseru do souradnic matrix
			const coords = this.transformCoordsInMatrix(e, computer);
			// kontrola pritomnosti ikon v prijatych souradnicich, pokud je, podle jejiho typu se bud' vymaze nebo se kratce zvyrazni cervene
			const check = this.checkUselessCell(coords);
			// kdyz ikonka neni, nastavi marker prazdne bunky
			if (check) {
				this.showIcons(this.opponent, coords, 'shaded-cell');
			}
		}

		checkUselessCell(coords) {
			// pri nastaveni markeru hracem, kdyz hodnota matrix je jina nez 0, predpoklada, ze tom miste je ikonka
			if (computer.matrix[coords[0]][coords[1]] > 1) return false;

			// получаем коллекцию маркеров на игровом поле противника
			// ziskavani kolekci hernich markeru na hernim pole soupere
			const icons = this.opponent.field.querySelectorAll('.shaded-cell');
			if (icons.length == 0) return true;

			for (let icon of icons) {
				// souradnice ikonky a jejich srovnani s argumentem funkce
				const [x, y] = Controller.getCoordsIcon(icon);
				if (coords[0] == x && coords[1] == y) {
					// pokud souradnice ikonky a souradnice z argumentu jsou stejny, provede check na to, jaka funkce vyvolala funkci checkUselesscell
					const f = (new Error()).stack.split('\n')[2].trim().split(' ')[1];
					if (f == 'Controller.setUselessCell') {
						// vymazani markeru prazdne bunky
						icon.remove();
					} else {
						// na 0.5s zabarvi marker nacerveno
						icon.classList.add('shaded-cell_red');
						setTimeout(() => { icon.classList.remove('shaded-cell_red') }, 500);
					}
					return false;
				}
			}
			return true;
		}

		// markery kolem lodi pri zasahu
		markUselessCell(coords) {
			let n = 1, x, y;

			for (let coord of coords) {
				x = coord[0]; y = coord[1];
				// souradnice za hernim polem
				if (x < 0 || x > 9 || y < 0 || y > 9) continue;
				// souradnice odpovidajici minuti nebo prazdnemu bunce
				if (human.matrix[x][y] == 2 || human.matrix[x][y] == 3) continue;
				// dopisovani hodnoty odpovidajici prazdne bunce nebo minuti
				human.matrix[x][y] = 2;
				// marker prazdne bunky do ziskanych souradnic tak, aby se vypisovali jeden za druhym
				// pri kazde iteraci zvysuje pauzu pred vypisovanim markeru
				setTimeout(() => this.showIcons(human, coord, 'shaded-cell'), 350 * n);
				// mazani souradnic ze vsech arrays
				this.removeCoordsFromArrays(coord);
				n++;
			}
		}

		transformCoordsInMatrix(e, self) {
			const x = Math.trunc((e.pageY - self.fieldTop) / Field.SHIP_SIDE);
			const y = Math.trunc((e.pageX - self.fieldLeft) / Field.SHIP_SIDE);
			return [x, y];
		}

		removeCoordsFromArrays(coords) {
			if (this.coordsAroundHit.length > 0) {
				this.coordsAroundHit = Controller.removeElementArray(this.coordsAroundHit, coords);
			}
			if (this.coordsFixedHit.length > 0) {
				this.coordsFixedHit = Controller.removeElementArray(this.coordsFixedHit, coords);
			}
			this.coordsRandomHit = Controller.removeElementArray(this.coordsRandomHit, coords);
		}

		// marker po zniceni lodi
		markUselessCellAroundShip(){
			// udeleni odpovidajicich hodnot z tempShip do promennych
			const {hits, kx, ky, x0, y0} = this.tempShip;
			let coords;

			// vypocet souradnich prazdnych bunek
			// lod s jednou palubou
			if (this.tempShip.hits == 1) {
				coords = [
					// horni
					[x0 - 1, y0],
					// dolni
					[x0 + 1, y0],
					// leva
					[x0, y0 - 1],
					// prava
					[x0, y0 + 1]
				];
			// lod s vice paluby
			} else {
				coords = [
					// leva / horni
					[x0 - kx, y0 - ky],
					// prava / dolni
					[x0 + kx * hits, y0 + ky * hits]
				];
			}
			this.markUselessCell(coords);
		}

		showIcons(opponent, [x, y], iconClass) {
			//instance herniho pole do ktereho bude umistena ikonka
			const field = opponent.field;
			// pauza mezi zasahem a minutim
			if (iconClass === 'dot' || iconClass === 'red-cross') {
				setTimeout(() => fn(), 400);
			} else {
				fn();
			}
			function fn() {
				// vytvoreni elementu a pridani do neho tridy a stilu
				const span = document.createElement('span');
				span.className = `icon-field ${iconClass}`;
				span.style.cssText = `left:${y * Field.SHIP_SIDE}px; top:${x * Field.SHIP_SIDE}px;`;
				// umisteni ikonky na herni pole
				field.appendChild(span);
			}
		}

		showExplosion(x, y) {
			this.showIcons(this.opponent, [x, y], 'explosion');
			const explosion = this.opponent.field.querySelector('.explosion');
			explosion.classList.add('active');
			setTimeout(() => explosion.remove(), 430);
		}

		getCoordsForShot() {
			const coords = (this.coordsAroundHit.length > 0) ? this.coordsAroundHit.pop() : (this.coordsFixedHit.length > 0) ? this.coordsFixedHit.pop() : this.coordsRandomHit.pop();			
			// mazani dosazenych souradnic ze vsech arrays
			this.removeCoordsFromArrays(coords);
			return coords;
		}

		resetTempShip() {
			this.tempShip = {
				hits: 0,
				firstHit: [],
				kx: 0,
				ky: 0
			};
		}

		makeShot(e) {
			let x, y;
			// pokud udalost existuje, byl to vystrel hrace
			if (e !== undefined) {
				// pokud to nebyl levy klik nebo je nastaven flag compShot, ma strilet pocitac
				if (e.which != 1 || compShot) return;
				// souradnice vystrelu podle souradnic v matrixu
				([x, y] = this.transformCoordsInMatrix(e, this.opponent));

				// overeni ikonky 'shaded-cell' podle dosazenych souradnic
				const check = this.checkUselessCell([x, y]);
				if (!check) return;
			} else {
				// souradnice pro vystrel pc
				([x, y] = this.getCoordsForShot());
			}

			// ukazovani a smazani ikonky vystrelu
			this.showExplosion(x, y);

			const v	= this.opponent.matrix[x][y];
			switch(v) {
				case 0: // minuti
					this.miss(x, y);
					break;
				case 1: // zasah
					this.hit(x, y);
					break;
				case 3: // opacny vystrel
				case 4:
					Controller.showServiceText('Už jsi sem střílel');
					break;
			}
		}

		miss(x, y) {
			let text = '';
			// nastaveni ikonky minuti a zapis do matrixu
			this.showIcons(this.opponent, [x, y], 'dot');
			this.opponent.matrix[x][y] = 3;

			// ziskani statusu hercu
			if (this.player === human) {
				text = 'Minul jsi. Počítač střílí.';
				this.player = computer;
				this.opponent = human;
				compShot = true;
				setTimeout(() => this.makeShot(), 2000);
			} else {
				text = 'Počítač minul. Ty střílíš.';

				// vsechny mozne bunky lode jsou prostrelene
				if (this.coordsAroundHit.length == 0 && this.tempShip.hits > 0) {
					// lod je potopena, kolem ni je UselessCell
					this.markUselessCellAroundShip();
					this.resetTempShip();
				}
				this.player = human;
				this.opponent = computer;
				compShot = false;
			}
			setTimeout(() => Controller.showServiceText(text), 400);
		}

		hit(x, y) {
			let text = '';
			// ikonka zasahu a zapis zasahu do matrix
			this.showIcons(this.opponent, [x, y], 'red-cross');
			this.opponent.matrix[x][y] = 4;
			// text o zasahu
			text = (this.player === human) ? 'Gratulujeme, zásah! Ty střílíš.' : 'Počítač zasáhl tvoji loď. Střílí počítač.';
			setTimeout(() => Controller.showServiceText(text), 400);

			// trideni lodi nepratelske eskadry
			outerloop:
			for (let name in this.opponent.squadron) {
				const dataShip = this.opponent.squadron[name];
				for (let value of dataShip.arrDecks) {
					// kontrola souřadnic paluby a srovnani ji se zasahem, pokud se neshoduji, dalsi iterace
					if (value[0] != x || value[1] != y) continue;
					dataShip.hits++;
					if (dataShip.hits < dataShip.arrDecks.length) break outerloop;
					// kod pro vystrel pocitace, ukladani souradnic prvni paluby
					if (this.opponent === human) {
						this.tempShip.x0 = dataShip.x;
						this.tempShip.y0 = dataShip.y;
					}
					// kdyz pocet zasahu se rovna poctu palub, odstranuje pocitac z eskadry
					delete this.opponent.squadron[name];
					break outerloop;
				}
			}

			// vse lode byli znicene
			if (Object.keys(this.opponent.squadron).length == 0) {
				if (this.opponent === human) {
					text = 'Bohužel, jsi prohrál.';
					// ukazovani zbyvajicich lodi
					for (let name in computer.squadron) {
						const dataShip = computer.squadron[name];
						Ships.showShip(computer, name, dataShip.x, dataShip.y, dataShip.kx );
					}
				} else {
					text = 'Поздравляем! Вы выиграли!';
				}
				Controller.showServiceText(text);
				// ukazovani tlacitka nove hry
				buttonNewGame.hidden = false;
			// pokracovani hry
			} else if (this.opponent === human) {
				let coords;
				this.tempShip.hits++;

				// oznaceni diagonalnich bunek, kde lod byt nemuze
				coords = [
					[x - 1, y - 1],
					[x - 1, y + 1],
					[x + 1, y - 1],
					[x + 1, y + 1]
				];
				this.markUselessCell(coords);

				// vytvoreni ostrelovacich souradnic kolem zasahu
				coords = [
					[x - 1, y],
					[x + 1, y],
					[x, y - 1],
					[x, y + 1]
				];
				this.setCoordsAroundHit(x, y, coords);

				// check na potopenou lod
				this.isShipSunk();

				// pauza mezi vystrely
				setTimeout(() => this.makeShot(), 2000);
			}
		}
	}

	///////////////////////////////////////////

	const instruction = getElement('instruction');
	const shipsCollection = getElement('ships_collection');
	const initialShips = document.querySelector('.wrap + .initial-ships');
	const toptext = getElement('text_top');
	const buttonPlay = getElement('play');
	const buttonNewGame = getElement('newgame');

	// instance hraciho pole cloveka
	const human = new Field(humanfield);

	let computer = {};

	let control = null;

	getElement('type_placement').addEventListener('click', function(e) {
		// delegovani na zaklade udalosti
		if (e.target.tagName != 'SPAN') return;

		buttonPlay.hidden = true;
		// vycisteni hraciho pole pred opakovanym nastavenim lodi na pole
		human.cleanField();

		// ocisteni klonu objektu s eskadrou
		let initialShipsClone = '';
		// zpusob nastaveni lod na herni pole
		const type = e.target.dataset.target;
		
		const typeGeneration = {
			random() {
				// skryti kontejneru s lodemi, ze ktereho se pretahuji na herni pole
				shipsCollection.hidden = true;
				// call funkce nahodneho umistnei loodi
				human.randomLocationShips();
			},
			manually() {
				// viditelnost kolekci lodi
				let value = !shipsCollection.hidden;

				// pokud v kontejneru krome informacniho radku jsou lode, smaze je
				if (shipsCollection.children.length > 1) {
					shipsCollection.removeChild(shipsCollection.lastChild);
				}

				// pokud kolekce lodi pri kliku na pseudolink neni videt, kopiruje ho a prenasi do herniho kontejneru a na obrazovku
				if (!value) {
					initialShipsClone = initialShips.cloneNode(true);
					shipsCollection.appendChild(initialShipsClone);
					initialShipsClone.hidden = false;
				}

				// podle dosazeneho value ukazuje nebo skryva block s kolekci lodi
				shipsCollection.hidden = value;
			}
		};
		// call funkci podle druhu generaci
		typeGeneration[type]();

		// instance tridy pro pretahovani a upravovani polohy lodi
		const placement = new Placement();
		// observer udalosti
		placement.setObserver();
	});

	buttonPlay.addEventListener('click', function(e) {
		// skryti nepotrebnych elementu pro hru
		buttonPlay.hidden = true;
		instruction.hidden = true;
		// show herni pole pc
		computerfield.parentElement.hidden = false;
		toptext.innerHTML = 'Lodě';

		// instance herniho pole pc
		computer = new Field(computerfield);
		// vyprazdneni pole od predchodzich lodi
		computer.cleanField();
		computer.randomLocationShips();
		// flag startu hry
		startGame = true;

		// instance kontrolera hry
		if (!control) control = new Controller();
		// spusk hry
		control.init();
	});

	buttonNewGame.addEventListener('click', function(e) {
		// skryti tlacitka restart
		buttonNewGame.hidden = true;
		// skryti herniho pole pc
		computerfield.parentElement.hidden = true;
		// ovladaci elementy nastaveni instrukci
		instruction.hidden = false;
		// ocisteni pole hrace
		human.cleanField();
		toptext.innerHTML = 'Расстановка кораблей';
		Controller.SERVICE_TEXT.innerHTML = '';

		// nastaveni vlajek do puvodniho stavu
		startGame = false;
		compShot = false;

		// vyprazdneni souradnic vystrelu
		control.coordsRandomHit = [];
		control.coordsFixedHit = [];
		control.coordsAroundHit = [];
		// vyprazdnit tempShip
		control.resetTempShip();
	});

	/////////////////////////////////////////////////

	function printMatrix() {
		let print = '';
		for (let x = 0; x < 10; x++) {
			for (let y = 0; y < 10; y++) {
				print += human.matrix[x][y];
			}
			print += '<br>';
		}
		getElement('matrix').innerHTML = print;
	}
})();
